import pathlib

from setuptools import setup, find_packages

from schoolrating import __version__


def requires(extra=''):
    file_path = pathlib.Path(__file__).parent / \
                'requirements{}.txt'.format('-' + extra if extra else '')
    with open(str(file_path)) as fh:
        return fh.read().split()


def read(filename):
    file_path = pathlib.Path(__file__).parent / filename
    with open(str(file_path), encoding='utf-8') as _fh:
        return _fh.read()


setup(
    name='schoolrating',
    version=__version__,
    description='School rating system.',
    # long_description=read(),
    classifiers=[
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
    ],
    keywords='fastapi rating school API',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires(),
)
