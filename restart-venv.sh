#!/usr/bin/env bash
# Stop the script when an error is raised.
set -e

# Remove and create our virtual environment form zero.
rm -fr venv
python -m venv venv

# Install basic packages.
venv/bin/pip install -U pip setuptools wheel requests alembic

# Install the package with their dependencies using the `setup.py`.
venv/bin/pip install -e .
