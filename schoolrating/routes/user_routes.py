from typing import List
from fastapi import APIRouter, HTTPException

from schoolrating.schemas import user_schema
from schoolrating.crud import users_crud

router = APIRouter(
    prefix="/users",
    tags=["Users"],
    dependencies=[],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[user_schema.User])
async def get_all_users():
    return await users_crud.get_user()


@router.post("/", response_model=user_schema.User)
async def add_user(new_user: user_schema.User) -> List[user_schema.User]:
    user = await users_crud.create_user(username=new_user.username,
                                        email=new_user.email)
    if user:
        return user
    raise HTTPException(status_code=404, detail="User not found")


# Todo: TBD
# @router.get("/{user_id}", response_model=user_schema.User)
# async def inspect_user(user_id: str) -> user_schema.User:
#     user = await users_crud.get_user(user_id=user_id)
#     if user:
#         return user_schema.User.from_orm(user)
#     raise HTTPException(status_code=404, detail="User not found")
