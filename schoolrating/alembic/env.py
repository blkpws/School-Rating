from alembic import context

from schoolrating import db


def run_migrations_offline():
    context.configure(url=db.DATABASE_URL)
    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    connection = db.engine.connect()
    context.configure(
        connection=connection,
        target_metadata=db.metadata
    )

    try:
        with context.begin_transaction():
            context.run_migrations()
    finally:
        connection.close()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
