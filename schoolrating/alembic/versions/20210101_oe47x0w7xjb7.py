""" init

Revision ID: oe47x0w7xjb7
Revises: 
Create Date: 2021-01-01 00:00:00.01

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'oe47x0w7xjb7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('username', sa.String),
        sa.Column('email', sa.String),
    )
    op.create_index('users_index', 'users', ['username', 'email'])


def downgrade():
    op.drop_index('users_index', table_name='users')
    op.drop_table('users')
