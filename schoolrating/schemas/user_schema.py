from typing import Optional
from pydantic import BaseModel


class User(BaseModel):
    username: str
    email: Optional[str] = None

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "username": "Blackpaws",
                "email": "hello@blackpaws.me",
            }
        }
