import databases
import sqlalchemy


# Guide: https://fastapi.tiangolo.com/advanced/async-sql-databases/
DATABASE_URL = "sqlite:///./schoolrating.sqlite"

database = databases.Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()


engine = sqlalchemy.create_engine(
    str(database.url), connect_args={"check_same_thread": False})
metadata.create_all(engine)
