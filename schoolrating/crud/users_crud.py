from typing import Union, List, Optional

from orm import exceptions as orm_exceptions

from schoolrating.models.user_model import User


async def get_user(user_id=None) -> Optional[Union[User, List[User]]]:
    try:
        if user_id:
            return await User.objects.get(id=user_id)
        return await User.objects.all()
    except orm_exceptions.NoMatch:
        pass


async def create_user(username, email=None) -> User:
    return await User.objects.create(username=username, email=email)
