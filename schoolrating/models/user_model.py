import orm

from schoolrating import db


class User(orm.Model):
    __tablename__ = "users"
    __database__ = db.database
    __metadata__ = db.metadata
    id = orm.Integer(primary_key=True)
    username = orm.String(max_length=None, allow_blank=True)
    email = orm.String(max_length=None, allow_blank=True)
