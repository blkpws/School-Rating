from fastapi import FastAPI

import alembic.config

from schoolrating import db
from schoolrating.routes import user_routes

# Prepare database.
alembicArgs = ['upgrade', 'head']
alembic.config.main(argv=alembicArgs)

# Start API.
app = FastAPI(servers=[{"url": "/"}, {"url": "/api/v1"}],
              docs_url="/swagger", redoc_url='/redoc')

app.include_router(user_routes.router)


@app.on_event("startup")
async def startup():
    await db.database.connect()


@app.on_event("shutdown")
async def shutdown():
    await db.database.disconnect()


if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=6543)
