
### Initiate database with Alembic

This project uses Alembic to manage the database, there are a few commands you
need to type to get the database ready and up to date or checkout to an old state.

To init the database we will use the following command:

    venv/bin/alembic upgrade head
