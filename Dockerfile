FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim

WORKDIR /schoolrating
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
RUN pip install -e .

CMD ["uvicorn", "schoolrating.main:app", "--host", "0.0.0.0", "--port", "80"]
